package com.jaddicted.spring6webapp.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.jaddicted.spring6webapp.domain.Author;
import com.jaddicted.spring6webapp.domain.Book;
import com.jaddicted.spring6webapp.repositories.AuthorRepository;
import com.jaddicted.spring6webapp.repositories.BookRepository;

@Component
public class BootstrapData implements CommandLineRunner {
	private final AuthorRepository authorRepository;
	private final BookRepository bookRepository;

	public BootstrapData(AuthorRepository authorRepository, BookRepository bookRepository) {
		super();
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Author eric = new Author();
		eric.setFirstName("Eric");
		eric.setLastName("Evans");

		Book ddd = new Book();
		ddd.setTitle("Domain Driven Design");
		ddd.setIsbn("123456");
		
		Author ericSaved = authorRepository.save(eric);
		Book dddSaved = bookRepository.save(ddd);
		

		Author rod = new Author();
		eric.setFirstName("Rod");
		eric.setLastName("Johnson");

		Book noEJB = new Book();
		noEJB.setTitle("J2EE Development without EJB");
		noEJB.setIsbn("654321");
		
		Author rodSaved = authorRepository.save(rod);
		Book noEJBSaved = bookRepository.save(noEJB);
		
		
		ericSaved.getBooks().add(dddSaved);
		rodSaved.getBooks().add(noEJBSaved);
		

		authorRepository.save(ericSaved);
		authorRepository.save(rodSaved);
		
		System.out.println("In Bootstrap");
		System.out.println("Author count: " + authorRepository.count());
		System.out.println("Book count: "+bookRepository.count());
	}

}
