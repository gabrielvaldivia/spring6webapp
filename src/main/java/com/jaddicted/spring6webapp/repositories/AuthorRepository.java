package com.jaddicted.spring6webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jaddicted.spring6webapp.domain.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
